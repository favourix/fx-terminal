# Laotian translations for extra package.
# Copyright (C) 2018 THE extra'S COPYRIGHT HOLDER
# This file is distributed under the same license as the extra package.
# Automatically generated, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: extra\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-01-08 17:16+0000\n"
"PO-Revision-Date: 2018-03-11 14:30-0700\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: lo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/com.favourix.terminal.appdata.xml.in:8
#: data/com.favourix.terminal.desktop.in:3
#: data/com.favourix.terminal.desktop.in:4
msgid "Terminal"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:9
#: data/com.favourix.terminal.desktop.in:5
msgid "Use the command line"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:11
msgid ""
"Terminal is a terminal emulator application for accessing a UNIX shell "
"environment which can be used to run programs available on your system."
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:40
#: data/com.favourix.terminal.appdata.xml.in:54
msgid "New features:"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:42
msgid "Add commandline option for New Window (-n)"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:44
#: data/com.favourix.terminal.appdata.xml.in:59
msgid "Other updates:"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:46
msgid "Prevent duplicate tab on startup"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:47
msgid "Prevent unnecessary Home tab on startup"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:48
#: data/com.favourix.terminal.appdata.xml.in:63
#: data/com.favourix.terminal.appdata.xml.in:76
#: data/com.favourix.terminal.appdata.xml.in:85
#: data/com.favourix.terminal.appdata.xml.in:94
#: data/com.favourix.terminal.appdata.xml.in:104
#: data/com.favourix.terminal.appdata.xml.in:117
#: data/com.favourix.terminal.appdata.xml.in:127
#: data/com.favourix.terminal.appdata.xml.in:135
#: data/com.favourix.terminal.appdata.xml.in:143
#: data/com.favourix.terminal.appdata.xml.in:151
#: data/com.favourix.terminal.appdata.xml.in:159
#: data/com.favourix.terminal.appdata.xml.in:170
#: data/com.favourix.terminal.appdata.xml.in:178
#: data/com.favourix.terminal.appdata.xml.in:187
#: data/com.favourix.terminal.appdata.xml.in:196
#: data/com.favourix.terminal.appdata.xml.in:204
#: data/com.favourix.terminal.appdata.xml.in:219
msgid "Translation updates"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:56
msgid "Turn Natural Copy/Paste on or off in the Settings menu"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:57
msgid "Zoom with Ctrl + Scroll"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:61
msgid "Include sudo password prompt when copying last output"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:62
msgid "Apply font changes without restarting"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:70
msgid "Don't remember tabs when History is disabled in System Settings"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:71
msgid "Show keyboard shortcuts in the secondary-click menu"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:72
msgid "Middle-click paste now properly follows System Settings"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:73
msgid "Shift + Control + D to duplicate tabs"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:74
msgid "Use neutral colors for the dark style scheme"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:75
msgid "Slightly pad text from the edge of the window"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:83
msgid "Pressing the Menu key now opens the context menu"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:84
msgid "Store state with GSettings"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:92
msgid "Support Ctrl+Equal key combo to zoom in"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:93
msgid "Fix a wrong foreground process response code"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:101
msgid "Return focus after popover menu closes"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:102
msgid "Prevent search and style buttons from receiving focus"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:103
msgid "Hide the cursor while typing"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:111
msgid "Fix erratic search text highlightning behavior"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:112
msgid "Support -h flag for help"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:113
msgid "Disable audible bell"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:114
msgid "Add Alt+↑ action to scroll to last command"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:115
msgid "Add Alt+c action to copy last output"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:116
msgid "Add accels to tooltips"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:124
msgid "Fix zsh notifications"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:125
msgid "Conditional ctrl+g shortcut to deconflict with emacs"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:126
#: data/com.favourix.terminal.appdata.xml.in:134
#: data/com.favourix.terminal.appdata.xml.in:142
#: data/com.favourix.terminal.appdata.xml.in:150
#: data/com.favourix.terminal.appdata.xml.in:158
#: data/com.favourix.terminal.appdata.xml.in:169
#: data/com.favourix.terminal.appdata.xml.in:177
#: data/com.favourix.terminal.appdata.xml.in:186
#: data/com.favourix.terminal.appdata.xml.in:195
#: data/com.favourix.terminal.appdata.xml.in:203
#: data/com.favourix.terminal.appdata.xml.in:218
msgid "Minor bug fixes"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:166
msgid "FISH integration fixes"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:167
msgid "Theme preference menu"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:168
msgid "Zoom level controls"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:185
msgid "Allow tab to close after typing exit at the commandline"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:194
msgid "Fix copying URLs to clipboard"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:211
msgid "Fix appstream date format"
msgstr ""

#: data/com.favourix.terminal.appdata.xml.in:253
msgid "Favourix."
msgstr ""

#. TRANSLATORS: Do NOT translate or transliterate this text (this is an icon file name)!
#: data/com.favourix.terminal.desktop.in:9
msgid "utilities-terminal"
msgstr ""

#: data/com.favourix.terminal.desktop.in:13
msgid "command;prompt;cmd;commandline;run;"
msgstr ""

#: data/com.favourix.terminal.desktop.in:18
msgid "New Window"
msgstr ""

#: data/com.favourix.terminal.desktop.in:22
msgid "New Root Tab"
msgstr ""
